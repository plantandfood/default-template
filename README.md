# README #

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Project Description
Name  | Value  | Description
----- | ------ | -------------------------------------------------------
Title	|		|Title for this repository - can differ from Project Title as in SAP 
Project code	|	P/000000/00		|SAP Project code or search keyword. Use P/000000/00 if there is no code
Project Title	|			|Project Title from SAP
Keywords	|			|e.g. crop species, insect species, pathogen, etc., as in a journal paper, comma separated list
Project leader	|			|Project leader for the code as entered above and as in SAP, full name all caps
Lead researcher	|			|		Person leading this piece of research- may be the same as project leader
Data custodian	|			|		Person responsible for managing the data (not necessarily the Data Steward)
Data specialist	|			|		Statistician/Biometrician/Bioinformatician/ other informatician…
Business Manager	|			|		Business Manager for the code as entered above and as in SAP	
Other project codes	|			|		List of other codes that this data will be used for- may be empty
Funding source	|			|		e.g. SSIF, Commercial , as listed in SAP
Project Start Date	|			|		As in SAP
Project End Date	|			|		As in SAP
Other researchers	|			|		Other researchers and contributing staff
Tracking codes	|			|		Milestones or Objective numbers
Location	|			|		Such as PFR office, field location (GPS, or description, or??), Region…
Description	|			|		Free-form description of study/ trial. 		

* create a machine readable version of the metadata in the project_metadata.yml or .json files.

### Documentation
* Issue tracking
  * Jira: 
* Documentation
  * Confluence: 


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact